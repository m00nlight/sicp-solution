;; Section 3.1.1

(define balance 100)

(define (withdraw amount)
  (if (>= balance amount)
      (begin
	(set! balance (- balance amount))
	balance)
      "Insufficient funds"))

(withdraw 25)
;Value: 75

(withdraw 50)
;Value: 25

(withdraw 30)
;Value 14: "Insufficient funds"


(define new-withdraw
  (let ((balance 100))
    (lambda (amount)
      (if (>= balance amount)
	  (begin
	    (set! balance (- balance amount))
	    balance)
	  "Insufficient funds"))))

(new-withdraw 25)
;Value: 75

(new-withdraw 50)
;Value: 25

(new-withdraw 50)
;Value 15: "Insufficient funds"

(define (make-withdraw balance)
  (lambda (amount)
    (if (>= balance amount)
	(begin
	  (set! balance (- balance amount))
	  balance)
	"Insufficient funds")))

(define W1 (make-withdraw 100))
(define W2 (make-withdraw 100))

(W1 50)
;Value: 50

(W2 70)
;Value: 30

(W2 40)
;Value 16: "Insufficient funds"

(W1 40)
;Value: 10


(define (make-account balance)
  (define (withdraw amount)
    (if (>= balance amount)
	(begin
	  (set! balance (- balance amount))
	  balance)
	"Insufficient funds"))
  (define (deposit amount)
    (begin
      (set! balance (+ balance amount))
      balance))
  (define (dispatch m)
    (cond ((eq? m 'withdraw) withdraw)
	  ((eq? m 'deposit) deposit)
	  (else (error "Unknow request: MAKE-ACCOUNT" m))))
  dispatch)

(define acc (make-account 100))
((acc 'withdraw) 50)
;Value: 50
((acc 'withdraw) 60)
;Value 17: "Insufficient funds"
((acc 'deposit) 40)
;Value: 90


;; Section 3.1.2

(define (estimate-pi trials)
  (sqrt (/ 6 (monte-carlo trials cesaro-test))))

(define (cesaro-test)
  (= (gcd (random 1000000) (random 1000000)) 1))

(define (monte-carlo trials experiment)
  (define (iter trials-remaining trials-passed)
    (cond ((= trials-remaining 0) (/ trials-passed trials))
	  ((experiment) (iter (- trials-remaining 1) (+ trials-passed 1)))
	  (else (iter (- trials-remaining 1) trials-passed))))
  (iter trials 0))

(estimate-pi 500000)
;Value: 3.141946965021699

;; Section 3.1.3

(define (make-simplified-withdraw balance)
  (lambda (amount)
    (set! balance (- balance amount))
    balance))

(define W (make-simplified-withdraw 25))

(W 20)
;Value: 5
(W 10)
;Value: -5

(define (make-decrementer balance)
  (lambda (amount)
    (- balance amount)))

(define D (make-decrementer 25))
(D 10)
;Value: 15

(D 20)
;Value: 5


;; Section 3.3.1

(define x (list 'a 'b))
(define z1 (cons x x))
(define z2 (cons (list 'a 'b) (list 'a 'b)))

(define (set-to-wow! x) (set-car! (car x) 'wow) x)
z1
;Value 23: ((a b) a b)
(set-to-wow! z1)
z1
;Value 23: ((wow b) wow b)

z2
;Value 24: ((a b) a b)
(set-to-wow! z2)
;Value 24: ((wow b) a b)


(define (my-cons x y)
  (define (set-x! v) (set! x v))
  (define (set-y! v) (set! y v))
  (define (dispatch m)
    (cond ((eq? m 'my-car) x)
	  ((eq? m 'my-cdr) y)
	  ((eq? m 'my-set-car!) set-x!)
	  ((eq? m 'my-set-cdr!) set-y!)
	  (else
	   (error "Undefined operation: CONS" m))))
  dispatch)

(define (my-car z) (z 'my-car))
(define (my-cdr z) (z 'my-cdr))
(define (my-set-car! z new-value)
  ((z 'my-set-car!) new-value) z)
(define (my-set-cdr! z new-value)
  ((z 'my-set-cdr!) new-value) z)

;; Section 3.3.2

(define (front-ptr queue) (car queue))
(define (rear-ptr queue) (cdr queue))
(define (set-front-ptr! queue item) (set-car! queue item))
(define (set-rear-ptr! queue item) (set-cdr! queue item))

(define (empty-queue? queue) (null? (front-ptr queue)))

(define (make-queue) (cons '() '()))

(define (front-queue queue)
  (if (empty-queue? queue)
      (error "FRONT called with an empty queue" queue)
      (car (front-ptr queue))))

(define (insert-queue! queue item)
  (let ((new-pair (cons item '())))
    (cond ((empty-queue? queue)
	   (set-front-ptr! queue new-pair)
	   (set-rear-ptr! queue new-pair)
	   queue)
	  (else (set-cdr! (rear-ptr queue) new-pair)
		(set-rear-ptr! queue new-pair)
		queue))))

(define (delete-queue! queue)
  (cond ((empty-queue? queue)
	 (error "DELETE! called with an empty queue" queue))
	(else
	 (set-front-ptr! queue (cdr (front-ptr queue)))
	 queue)))

(define q1 (make-queue))
(insert-queue! q1 'a)
;Value 13: ((a) a)
(insert-queue! q1 'b)
;Value 13: ((a b) b)
(delete-queue! q1)
;Value 13: ((b) b)
(delete-queue! q1)
;Value 13: (() b)


;; Section 3.3.3

(define (make-1d-table)
  (let ((local-table (list '*table*)))
    (define (lookup key)
      (let ((record (assoc key (cdr local-table))))
	(if record
	    (cdr record)
	    #f)))
    (define (insert! key value)
      (let ((record (assoc key (cdr local-table))))
	(if record
	    (set-cdr! record value)
	    (set-cdr! local-table (cons (cons key value) (cdr local-table))))
	'OK))
    (define (dispatch m)
      (cond ((eq? m 'lookup) lookup)
	    ((eq? m 'insert!) insert!)
	    (else (error "1D-TABLE Unknow operation" m))))
    dispatch))

(define t1 (make-1d-table))

((t1 'lookup) 'a)
;Value: #f

((t1 'insert!) 'a 3)
;Value: ok

((t1 'insert!) 'b 4)
;Value: ok

((t1 'lookup) 'a)
;Value: 3


(define (make-2d-table)
  (let ((local-table (list '*table*)))
    (define (lookup key1 key2)
      (let ((subtable (assoc key1 (cdr local-table))))
	(if subtable
	    (let ((record (assoc key2 (cdr subtable))))
	      (if record
		  (cdr record)
		  #f))
	    #f)))
    (define (insert! key1 key2 value)
      (let ((subtable (assoc key1 (cdr local-table))))
	(if subtable
	    (let ((record (assoc key2 (cdr subtable))))
	      (if record
		  (set-cdr! record value)
		  (set-cdr! subtable (cons
				      (cons key2 value)
				      (cdr subtable)))))
	    (set-cdr local-table (cons list key1 (cons key2 value)
				       (cdr local-table))))
	'OK))
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
	    ((eq? m 'insert-proc!) insert!)
	    (else (error "Unknow operation on 2D-TABLE" m))))
    dispatch))

(define operation-table (make-2d-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))


;; Section 3.4.1

(define (make-account balance)
  (define (withdraw amount)
    (if (> balance amount)
	(begin
	  (set! balance (- balance amount))
	  balance)
	"Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (let ((protected (make-serializer)))
    (define (dispatch m)
      (cond ((eq? m 'withdraw) (protected withdraw))
	    ((eq? m 'deposit) (protected deposit))
	    ((eq? m 'balance) balance)
	    (else (error "Unknow request: MAKE-ACCOUNT" m))))
    dispatch))


;;; Complexity of using multiple shared resources

(define (exchange account1 account2)
  (let ((difference (- (account1 'balance)
		       (account2 'balance))))
    ((account1 'withdraw) difference)
    ((account2 'deposit) difference)))


(define (make-account balance)
  (define (withdraw amount)
    (if (> balance amount)
	(begin
	  (set! balance (- balance amount))
	  balance)
	"Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (let ((balance-serializer (make-serializer)))
    (define (dispatch m)
      (cond ((eq? m 'withdraw) (protected withdraw))
	    ((eq? m 'deposit) (protected deposit))
	    ((eq? m 'balance) balance)
	    ((eq? m 'serizlier) balance-serializer) ; return the protect serializer
	    (else (error "Unknow request: MAKE-ACCOUNT" m))))
    dispatch))


(define (deposit account amount)
  (let ((s (account 'serializer))
	(d (account 'deposit)))
    ((s d) amount)))


(define (serialized-exchange account1 account2)
  (let ((serializer1 (account1 'serializer))
	(serializer2 (account2 'serializer)))
    ((serializer1 (serializer2 exchange)) account1 account2)))


;;; implementing serializers

(define (make-serializer)
  (let (mutex (make-mutex))
    (lambda (p)
      (define (serialized-p . args)
	(mutex 'acquire)
	(let ((val (apply p args)))
	  (mutex 'release)
	  val))
      serialized-p)))

(define (make-mutex)
  (let ((cell (list false)))
    (define (the-mutex m)
      (cond ((eq? m 'acquire)
	     (if (test-and-set! cell)
		 (the-mutex 'acquire))); retry
	    ((eq? m 'release) (clear! cell))))
    the-mutex))


;;;; section 3.5 Stream

(define (sum-primes a b)
  (define (iter count accum)
    (cond ((> count b) accum)
	  ((prime? count)
	   (iter (+ count 1)
		 (+ count accum)))
	  (else (iter (+ count 1) accum))))
  (iter a 0))

(define (sum-primes2 a b)
  (accumulate + 0 (filter prime? (enumerate-interval a b))))


;; implement Stream

(define (memo-proc proc)
  (let ((already-run? #f)
	(result #f))
    (lambda ()
      (if (not already-run?)
	  (begin (set! result (proc))
		 (set! already-run? true)
		 result)
	  result))))

(define (my-delay exp)
  (memo-proc (lambda () exp)))

(define (my-force delayed-object)
  (delayed-object))

(define the-empty-stream '())

(define (my-stream-null? s)
  (eq? s the-empty-stream))

(define (my-stream-cons a b)
  (cons a (my-delay b)))

(define (my-stream-car stream)
  (car stream))

(define (my-stream-cdr stream)
  (my-force (cdr stream)))


(define (my-stream-ref s n)
  (if (= n 0)
      (my-stream-car s)
      (my-stream-ref (my-stream-cdr s) (- n 1))))

(define (my-stream-map proc s)
  (if (my-stream-null? s)
      the-empty-stream
      (my-stream-cons (proc (my-stream-car s))
		      (my-stream-map proc (my-stream-cdr s)))))

(define (my-stream-filter pred stream)
  (cond ((my-stream-null? stream)
	 the-empty-stream)
	((pred (my-stream-car stream))
	 (my-stream-cons (my-stream-car stream)
			 (my-stream-filter pred
					   (my-stream-cdr stream))))
	(else (my-stream-filter pred (my-stream-cdr stream)))))

(define (my-stream-for-each proc s)
  (if (my-stream-null? s)
      'done
      (begin
	(proc (my-stream-car s))
	(my-stream-for-each proc (my-stream-cdr s)))))

(define (my-stream-enumerate-interval low high)
  (if (> low high)
      the-empty-stream
      (my-stream-cons
       low
       (my-stream-enumerate-interval (+ low 1) high))))

(define (display-line x)
  (newline)
  (display x))

(define (display-stream s)
  (my-stream-for-each display-line s))

(define test (my-stream-cons 3 (+ 3 3)))
;Value: test

(my-stream-car test)
;Value: 3

(my-stream-cdr test)
;Value 6


;;; section 3.5.2

(define (from n)
  (cons-stream n (from (+ n 1))))

(define nats (from 1))

(stream-head nats 10)
;Value 13: (1 2 3 4 5 6 7 8 9 10)


(define (divisible? x y) (= (remainder x y) 0))
(define (not-divisible? x y) (not (divisible? x y)))

(define no-sevens
  (stream-filter (lambda (x)
		   (not (divisible? x 7))) nats))

(stream-ref no-sevens 100)
;Value: 117


(define (sieve stream)
  (cons-stream (stream-car stream)
	       (sieve (stream-filter (lambda (x) (not-divisible? x (stream-car stream)))
				     (stream-cdr stream)))))

(define primes
  (sieve (from 2)))

(stream-ref primes 100000)
;Aborting!: out of memory
;Aborting!: out of memory
;Aborting!: out of memory  C-c C-c
;Quit!

(stream-head primes 20)
;Value 16: (2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71)

(define ones (cons-stream 1 ones))

(stream-head ones 10)
;Value 17: (1 1 1 1 1 1 1 1 1 1)

(define (add-stream s1 s2)
  (stream-map + s1 s2))

(define integers
  (cons-stream 1 (add-stream ones integers)))

(stream-head integers 20)
;Value 18: (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)


;; a better way to generate primes
(define primes
  (cons-stream 2 (stream-filter prime? (from 3))))

(define (prime? n)
  (define (iter ps)
    (cond ((> (square (stream-car ps)) n) #t)
	  ((divisible? n (stream-car ps)) #f)
	  (else (iter (stream-cdr ps)))))
  (iter primes))

(stream-ref primes 100000)
;Value: 1299721


;; Section 3.5.3

(define (average x y)
  (/ (+ x y) 2.0))

(define (sqrt-improve guess x)
  (average guess (/ x guess)))

(define (sqrt-stream x)
  (define guesses
    (cons-stream
     1.0
     (stream-map (lambda (guess) (sqrt-improve guess x)) guesses)))
  guesses)

(stream-head (sqrt-stream 2.0) 4)
;Value 13: (1. 1.5 1.4166666666666665 1.4142156862745097)


(define (partial-sum stream)
  (let ((hh (stream-car stream)))
    (cons-stream hh (stream-map (lambda (x) (+ x hh)) (partial-sum (stream-cdr stream))))))

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))


(define (pi-summands n)
  (cons-stream (/ 1.0 n)
	       (stream-map - (pi-summands (+ n 2)))))

(define pi-stream
  (scale-stream (partial-sum (pi-summands 1)) 4))

(stream-head pi-stream 4)
;Value 16: (4. 2.666666666666667 3.466666666666667 2.8952380952380956)


(define (euler-transform s)
  (let ((s0 (stream-ref s 0)) ;S(n-1)
	(s1 (stream-ref s 1)) ;S(n)
	(s2 (stream-ref s 2)));S(n+1)
    (cons-stream (- s2 (/ (square (- s2 s1))
			  (+ s0 (* -2 s1) s2)))
		 (euler-transform (stream-cdr s)))))

(stream-head (euler-transform pi-stream) 4)
;Value 17: (3.166666666666667 3.1333333333333337 3.1452380952380956 3.1396825396825396)

(define (make-tableau transform s)
  (cons-stream s (make-tableau transform (transform s))))

(define (accelerated-sequence transform s)
  (stream-map stream-car (make-tableau transform s)))

(stream-head (accelerated-sequence euler-transform pi-stream) 4)
;Value 18: (4. 3.166666666666667 3.142105263157895 3.1415993573190044)


;; Infinite streams of pairs

(define (from n)
  (cons-stream n (from (+ n 1))))

(define nats (from 0))


(define (interleave s1 s2)
  (if (stream-null? s1)
      s2
      (cons-stream (stream-car s1)
		   (interleave s2 (stream-cdr s1)))))

(define (pairs s t)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (interleave
    (stream-map (lambda (x) (list (stream-car s) x))
		(stream-cdr t))
    (pairs (stream-cdr s) (stream-cdr t)))))

(stream-head (pairs nats nats) 10)
;Value 15: ((0 0) (0 1) (1 1) (0 2) (1 2) (0 3) (2 2) (0 4) (1 3) (0 5))



;; Streams as signals


(define (add-stream s t)
  (stream-map + s t))

(define (scale-stream s factor)
  (stream-map (lambda (x) (* x factor)) s))

(define (integral integrand initial-value dt)
  (define int
    (cons-stream initial-value
		 (add-stream (scale-stream integrand dt)
			     int)))
  int)


;; Section 3.5.4 Streams and Delayed Evaluation
