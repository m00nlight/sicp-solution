;; Exercise 3.1

(define (make-accumulator value)
  (lambda (amount)
    (begin
      (set! value (+ value amount))
      value)))

(define A (make-accumulator 5))

(A 10)
;Value: 15

(A 10)
;Value: 25


;; Exercise 3.2
(define (make-monitored f)
  (let ((count 0))
    (lambda (params)
      (cond ((eq? params 'how-many-calls?) count)
	    ((eq? params 'reset-count)
	     (begin
	       (set! count 0)
	       "Reset count to zero"))
	    (else
	     (begin
	       (set! count (+ count 1))
	       (f params)))))))

(define s (make-monitored sqrt))
(s 100)
;Value: 10
(s 'how-many-calls?)
;Value: 1
(s 300)
;Value: 17.320508075688775
(s 'how-many-calls?)
;Value: 2
(s 'reset-count)
;Value 18: "Reset count to zero"
(s 'how-many-calls?)
;Value: 0


;; Exercise 3.3

(define (make-account balance passwd)
  (define (withdraw amount)
    (if (>= balance amount)
	(begin
	  (set! balance (- balance amount))
	  balance)
	"Insufficient funds"))
  (define (deposit amount)
    (begin
      (set! balance (+ balance amount))
      balance))
  (define (dispatch pwd m)
    (if (eq? pwd passwd)
	(cond ((eq? m 'withdraw) withdraw)
	      ((eq? m 'deposit) deposit)
	      (else (error "Unknow operation: MAKE-ACCOUNT" m)))
	(error "Incorrect password")))
  dispatch)

(define acc (make-account 100 'secret-password))

((acc 'secret-password 'withdraw) 40)
;Value: 60
((acc 'some-other-passwd 'deposit) 60)
;Incorrect password



;; Exercise 3.4

(define (make-account balance passwd)
  (let ((cc 7))
    (define (withdraw amount)
      (if (>= balance amount)
	  (begin
	    (set! balance (- balance amount))
	    balance)
	  "Insufficient funds"))
    (define (deposit amount)
      (begin
	(set! balance (+ balance amount))
	balance))
    (define (call-the-cops)
      (error "7 continus error, is you the true owner"))
    (define (dispatch pwd m)
      (if (eq? pwd passwd)
	  (begin
	    (set! cc 7)
	    (cond ((eq? m 'withdraw) withdraw)
		  ((eq? m 'deposit) deposit)
		  (else (error "Unknow operation: MAKE-ACCOUNT" m))))
	  (if (= cc 0)
	      (call-the-cops)
	      (begin
		(set! cc (- cc 1))
		(error "Incorrect password")))))
    dispatch))

(define acc (make-account 100 'secret-password))

((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
((acc 'some-other-passwd 'withdraw) 40)
;7 continus error, is you the true owner


;; Exercise 3.5

(define (monte-carlo trials experiment)
  (define (iter trials-remaining trials-passed)
    (cond ((= trials-remaining 0) (/ trials-passed trials))
	  ((experiment) (iter (- trials-remaining 1) (+ trials-passed 1)))
	  (else (iter (- trials-remaining 1) trials-passed))))
  (iter trials 0))

(define (random-in-range low high)
  (let ((range (- high low)))
    (+ low (random (* 1.0 range)))))

(define (estimate-integral x1 x2 y1 y2 trials)
  (define (pred)
    (let ((x (random-in-range x1 x2))
	  (y (random-in-range y1 y2)))
      (<= (+ (square (- x 5))
	     (square (- y 7))) 9)))
  (let ((prob (monte-carlo trials pred)))
    (/ (* prob (- x2 x1) (- y2 y1)) 9.0)))

(estimate-integral 2 8 4 10 100000)
;Value: 3.14996


;; Exercise 3.6 See wiki for detail

(define rand
  (let ((rand-state 1))
    (define (dispatch proc-name)
      (cond ((eq? proc-name 'generate)
	     (begin
	       (set! rand-state (+ rand-state 1))
	       rand-state))
	    ((eq? proc-name 'reset)
	     (lambda (new-value)
	       (set! rand-state new-value)
	       'RESET-DONE))))
    dispatch))

(rand 'generate)

((rand 'reset) 10)

(rand 'generate)

;; Exercise 3.7

(define (make-account balance passwd)
  (let ((passwd-list (cons passwd '())))
    (define (withdraw amount)
      (if (>= balance amount)
	  (begin
	    (set! balance (- balance amount))
	    balance)
	  "Insufficient funds"))
    (define (deposit amount)
      (begin
	(set! balance (+ balance amount))
	balance))
    (define (join new-passwd)
      (set! passwd-list (cons new-passwd passwd-list))
      'JOINT-ACCOUNT-DONE)
    (define (dispatch pwd m)
      (if (find (lambda (x) (eq? pwd x)) passwd-list)
	  (cond ((eq? m 'withdraw) withdraw)
		((eq? m 'deposit) deposit)
		((eq? m 'join) join)
		(else (error "Unknow operation: MAKE-ACCOUNT" m)))
	  (error "Incorrect password")))
    dispatch))

(define (make-join acc passwd new-passwd)
  (begin
    ((acc passwd 'join) new-passwd)
    acc))


(define peter-acc (make-account 100 'open-sesame))

(define paul-acc (make-join peter-acc 'wrong-passwd 'rosebud))
;Incorrect password

(define paul-acc (make-join peter-acc 'open-sesame 'rosebud))
;Value: paul-acc

((paul-acc 'rosebud 'withdraw) 10)
;Value: 90

;; Exercise 3.8

(define f
  (let ((s #f)
	(v -1))
    (lambda (val)
      (if s
	  v
	  (begin
	    (set! s #t)
	    (set! v (/ val 2))
	    v)))))

(+ (f 0) (f 1))
;Value: 1

(f 0)

(+ (f 0) (f 1))
;Value: 0


;;; Exercise 3.9  See wiki for detail

;;; Exercise 3.10 - 3.11 Leave for future


;; Exercise

(define (my-append! x y)
  (set-cdr! (my-last-pair x) y)
  x)

(define (my-last-pair x)
  (if (null? (cdr x)) x (my-last-pair (cdr x))))

(my-append! '(1 2 3 4) '(4 5 5 7))
;Value 17: (1 2 3 4 4 5 5 7)

(define x (list 'a 'b))
(define y (list 'c 'd))
(define z (append x y))
z
(cdr x)
;Value 19: (b)
(define w (my-append! x y))
w
(cdr w)
;Value 19: (b c d)


;; Exercise 3.13
(define (make-circle x)
  (set-cdr! (my-last-pair x) x)
  x)

(define z (make-circle (list 'a 'b 'c)))
z
;; infinite loop to print a b c


;; Exercise 3.14

(define (mystery x)
  (define (loop x y)
    (if (null? x)
	y
	(let ((temp (cdr x)))
	  (set-cdr! x y)
	  (loop temp x))))
  (loop x '()))

(define v (list 'a 'b 'c 'd))

(define w (mystery v))
w
;Value 22: (d c b a)

;; Exercise 3.15

```
#!scheme
(define x (list 'a 'b))
(define z1 (cons x x))
(define z2 (cons (list 'a 'b) (list 'a 'b)))

(define (set-to-wow! x) (set-car! (car x) 'wow) x)
z1
;Value 23: ((a b) a b)
(set-to-wow! z1)
z1
;Value 23: ((wow b) wow b)

z2
;Value 24: ((a b) a b)
(set-to-wow! z2)
;Value 24: ((wow b) a b)
```

Because the cons cell z1 both its car and cdr part point to the same list, so when we change the car part, its cdr part also changed. But z2, although its car and cdr part are same element, infact, they're not the same list. It can be showed by the following test

```
#!scheme
(eq? (list 'a 'b) (list 'a 'b))
;Value: #f
```

;; Exercise 3.15 See wiki

(define x (list 'a 'b))
(define z1 (cons x x))
(define z2 (cons (list 'a 'b) (list 'a 'b)))

(define (set-to-wow! x) (set-car! (car x) 'wow) x)
z1
;Value 23: ((a b) a b)
(set-to-wow! z1)
z1
;Value 23: ((wow b) wow b)

z2
;Value 24: ((a b) a b)
(set-to-wow! z2)
;Value 24: ((wow b) a b)

;; Exercise 3.16

(define (count-pair x)
  (if (not (pair? x))
      0
      (+ (count-pair (car x)) (count-pair (cdr x)) 1)))

(count-pair '(a b c))
;Value: 3

(define x (list 'a 'b))
(define z1 (cons x '()))
(set-cdr! z1 (cdr x))
(count-pair z1)
;Value: 4

(define p (cons 'a 'b))
(define q (cons p p))
(define z2 (cons q q))
(count-pair z2)
;Value: 7

(define z3 (make-circle (list 'a 'b 'c)))
; (count-pair z3) will infinitive loop


;; Exercise 3.17
(define (count-pair x)
  (let ((track '()))
    (define (traverse t)
      (if (not (pair? t))
	  0
	  (if (assoc t track)
	      0
	      (let ((ans1 (traverse (car t)))
		    (ans2 (traverse (cdr t))))
		(begin
		  (set! track (cons (list t #t) track))
		  (+ 1 ans1 ans2))))))
    (traverse x)))

(count-pair '(a b c))
;Value: 3
(count-pair z1)
;Value: 3
(count-pair z2)
;Value: 3
(count-pair z3)
;Aborting!: maximum recursion depth exceeded

;; Exercise 3.18 - Exercise 3.19

(define (check-circle? x)
  (let ((p1 x)
	(p2 x))
    (define (loop)
      (begin
	(set! p1 (cdr p1))
	(if (pair? (cdr p2))
	    (set! p2 (cdr (cdr p2)))
	    (set! p2 (cdr p2)))
	(cond ((null? p2) #f)
	      ((eq? p1 p2) #t)
	      (else (loop)))))
    (loop)))

(define z3 (make-circle (list 'a 'b 'c)))
(check-circle? z3)
;Value: #t

(check-circle? (list 'a 'b 'c))
;Value: #f


;; Exercise 3.20
(define x (my-cons 1 2))
(define z (my-cons x x))
(my-set-car! (my-cdr z) 17)
(my-car x)
;Value: 17


;; Exercise 3.21

(define q1 (make-queue))
(insert-queue! q1 'a)
;Value 13: ((a) a)
(insert-queue! q1 'b)
;Value 13: ((a b) b)
(delete-queue! q1)
;Value 13: ((b) b)
(delete-queue! q1)
;Value 13: (() b)

;; It does not insert the item twice, but print the queue-ptr and the acutal list to represent queue.
Since the rear-ptr point to the end of the list, so it seems that the end item is printed twice.

(define (print-queue queue)
  (cond ((empty-queue? queue)
	 (display "Empty Queue")
	 (newline)
	 'DONE)
	(else
	 (letrec ((head (front-ptr queue))
		  (rear (rear-ptr queue))
		  (f (lambda (pointer)
		       (if (eq? pointer rear)
			   (begin
			     (display (car pointer))
			     (display ")")
			     (newline))
			   (begin
			     (display (car pointer))
			     (display ",")
			     (f (cdr pointer)))))))
	   (display "(")
	   (f head)))))

(print-queue q1)
; Empty Queue
;Value: done

(insert-queue! q1 'a)
(print-queue q1)
;(a)
;Unspecified return value

(insert-queue! q1 'b)
(print-queue q1)
;(a,b)
;Unspecified return value


;; Exercise 3.22
(define (make-queue)
  (let ((front-ptr '())
	(rear-ptr '()))
    (define (dispatch m)
      (cond ((eq? m 'empty-queue?) (null? front-ptr))
	    ((eq? m 'insert-queue!)
	     (lambda (item)
	       (let ((new-pair (cons item '())))
		 (if (null? front-ptr)
		     (begin
		       (set! front-ptr new-pair)
		       (set! rear-ptr new-pair)
		       'INSERT-DONE)
		     (begin
		       (set-cdr! rear-ptr new-pair)
		       (set! rear-ptr new-pair)
		       'INSERT-DONE)))))
	    ((eq? m 'delete-queue!)
	     (if (null? front-ptr)
		 (error "DELETE on empty queue!")
		 (begin
		   (set! front-ptr (cdr front-ptr))
		   'DELETE-DONE)))
	    ((eq? m 'print-queue)
	     (letrec ((ptr front-ptr)
		      (f (lambda (pointer)
			   (if (eq? pointer rear-ptr)
			       (begin
				 (display (car pointer))
				 (display ")")
				 (newline)
				 'DONE)
			       (begin
				 (display (car pointer))
				 (display ",")
				 (f (cdr pointer)))))))
	       (begin
		 (display "(")
		 (f ptr))))))
    dispatch))

(define q2 (make-queue))
(q2 'empty-queue?)
;Value: #t

((q2 'insert-queue!) 'a)
(q2 'print-queue)
; (a)
;Value: done

((q2 'insert-queue!) 'b)
;Value: insert-done
(q2 'print-queue)
;(a,b)
;Value: done

(q2 'delete-queue!)
;Value: delete-done

(q2 'print-queue)
;(b)
;Value: done



;; Exercise 3.23
(define (make-deque)
  (cons '() '()))
(define (deque-front deque) (car deque))
(define (deque-rear deque) (cdr deque))

(define (empty-deque? deque)
  (or (null? (deque-front deque))
      (null? (deque-rear deque))))


(define (front-insert-deque! deque item)
  (let ((node (if (empty-deque? deque)
		  (list item '() '())
		  (list item '() (deque-front deque)))))
    (if (empty-deque? deque)
	(begin (set-car! deque node) (set-cdr! deque node))
	(set-car! deque node))
    deque))

(define (front-delete-deque! deque)
  (if (empty-deque? deque)
      (error "FRONT-DELETE in empty deque" deque)
      (let ((node (deque-front deque)))
	(set-car! deque (caddr node))
	deque)))

(define (rear-insert-deque! deque item)
  (let ((node (if (empty-queue? deque)
		  (list item '() '())
		  (list item (deque-rear deque) '()))))
    (if (empty-deque? deque)
	(begin (set-car! deque node) (set-cdr! deque node))
	(set-cdr! deque node))
    deque))

(define (rear-delete-deque! deque)
  (if (empty-deque? deque)
      (error "REAR-DELETE in empty deque" deque)
      (let ((node (deque-rear deque)))
	(set-cdr! deque (cadr node))
	deque)))

(define dq (make-deque))

(front-insert-deque! dq 'a)
;Value 15: ((a () ()))

(front-insert-deque! dq 'b)
;Value 16: ((b () (a () ())) a () ())

(rear-insert-deque! dq 'c)
;Value 16: ((b () (a () ())) c (a () ()) ())

(rear-delete-deque! dq)
;Value 16: ((b () (a () ())) a () ())

(front-delete-deque! dq)
;Value 16: ((a () ()) a () ())

;; Exercise 3.24 See wiki for explanation

(assoc '(a b) '(((a b) . 1) ((c d) . 2)) equal?)
;Value 13: ((a b) . 1)

(assoc '(a b) '(((a b) . 1) ((c d) . 2)) eq?)
;Value: #f



;; Exercise 3.25

(define (make-general-table)
  (let ((local-table (list '*table*)))
    (define (lookup key-list)
      (let ((record (assoc key-list (cdr local-table))))
	(if record
	    (cdr record)
	    #f)))
    (define (insert! key-list value)
      (let ((record (assoc key-list (cdr local-table))))
	(if record
	    (set-cdr! record value)
	    (set-cdr! local-table (cons
				   (cons key-list value)
				   (cdr local-table))))
	'OK))
    (define (dispatch m)
      (cond ((eq? m 'lookup) lookup)
	    ((eq? m 'insert!) insert!)
	    (else (error "1D-TABLE Unknow operation" m))))
    dispatch))

(define gt (make-general-table))
((gt 'insert!) '(a b c) 4)
;Value: ok

((gt 'insert!) '(a b) 5)
;Value: ok

((gt 'lookup) '(a b))
;Value: 5


;; Exercise 3.26 See wiki


;; Exercise 3.27
(define (fib n)
  (cond ((= n 0) 0)
	((= n 1) 1)
	(else (+ (fib (- n 1))
		 (fib (- n 2))))))

(fib 30)
;Value: 832040
;About 2 second's for the answer appear

(define memo-fib
  (memoize (lambda (n)
	     (cond ((= n 0) 0)
		   ((= n 1) 1)
		   (else (+ (memo-fib (- n 1))
			    (memo-fib (- n 2))))))))

(define (memoize f)
  (let ((table (make-1d-table)))
    (lambda (x)
      (let ((previously-computed-result (1d-table/get table x #f)))
	(or previously-computed-result
	    (let ((result (f x)))
	      (1d-table/put! table x result)
	      result))))))

(memo-fib 30)
;Value: 832040
;Immediately return



;; Exercise 3.38 See wiki for detail

;; Exercise 3.39 See wiki for detail

;; Exercise 3.40 See wiki for detail

;; Exercise 3.41 See wiki for detail

;; Exercise 3.42 See wiki for detail

;; Exercise 3.43 See wiki for detail

;; Exercise 3.44 See wiki for detail

;; Exercise 3.45 See wiki for detail

;; Exercise 3.46 See wiki for detail


;; Exercise 3.47

;;; based on mutexes

(define (make-sempahore n)
  (let ((count n)
	(mutex (make-mutex)))
    (define (sempahore m)
      (cond ((eq? m 'acquire)
	     (mutex 'acquire)
	     (if (zero? count)
		 (begin
		   (mutex 'release)
		   (sempahore 'acquire)) ;; semphaore is full
		 (begin
		   (set! count (- count 1))
		   (mutex 'release))))
	    ((eq? m 'release)
	     (mutex 'acquire)
	     (set! count (+ count 1))
	     (mutex 'release))))
    sempahore))

;;; based on test-and-set!

;;; test-and-set! should be atomic
(define (test-and-set! cell)
  (if (car cell)
      #t
      (begin
	(set-car! cell true)
	#f)))


(define (make-sempahore n)
  (let ((lock (list #f))
	(count 0))
    (define (sempahore m)
      (cond ((eq? m 'acquire)
	     (acquire-lock)
	     (if (= count n)
		 (begin
		   (release-lock)
		   (sempahore 'acquire))
		 (begin
		   (set! count (+ count 1))
		   (release-lock))))
	    ((eq? m 'release)
	     (acquire-lock)
	     (set! count (- count 1))
	     (release-lock))))
    (define (acquire-lock)
      (if (test-and-set! lock)
	  (acquire-lock)))
    (define (clear!)
      (set-car! lock #f))
    semaphore))


;; Exercise 3.50
(define (my-stream-map2 proc . argstreams)
  (if (null? (car argstreams))
      the-empty-stream
      (my-stream-cons
       (apply proc (map my-stream-car argstreams))
       (apply my-stream-map2
	      (cons proc (map my-stream-cdr argstreams))))))

;; Exercise 3.51

(define (show x)
  (display-line x)
  x)

(define x
  (my-stream-map
   show
   (my-stream-enumerate-interval 0 10)))
;; 10
;; 9
;; 8
;; 7
;; 6
;; 5
;; 4
;; 3
;; 2
;; 1
;; 0
;Value: x


(my-stream-ref x 5)
;Value: 5
(my-stream-ref x 7)
;Value: 7


;; Exercise 3.52

(define sum 0)
;Value: sum

(define (accum x)
  (set! sum (+ x sum))
  sum)
;Value: accum

(define seq
  (my-stream-map
   accum
   (my-stream-enumerate-interval 1 20)))
;Value: seq

(define y (my-stream-filter even? seq))
;Value: y

(define z
  (my-stream-filter
   (lambda (x)
     (= (remainder x 5) 0)) seq))
;Value: z

(my-stream-ref y 7)
;Value: 90

(display-stream z)
;; 210
;; 200
;; 195
;; 165
;; 155
;; 105
;; 90
;; 20
;; ;Value: done


;; Exercise 3.53
(define (add-stream s1 s2)
  (stream-map + s1 s2))

(define s (cons-stream 1 (add-stream s s)))

(stream-head s 10)
;Value 13: (1 2 4 8 16 32 64 128 256 512)

;; Exercise 3.54
(define (from n)
  (cons-stream n (from (+ n 1))))

(define nats (from 1))

(define (mul-streams s1 s2)
  (stream-map * s1 s2))

(define factorials
  (cons-stream 1 (mul-streams (stream-cdr nats) factorials)))

(stream-head factorials 10)
;Value 15: (1 2 6 24 120 720 5040 40320 362880 3628800)


;; Exercise 3.55

(define (partial-sum stream)
  (let ((hh (stream-car stream)))
    (cons-stream hh (stream-map (lambda (x) (+ x hh)) (partial-sum (stream-cdr stream))))))

(define rr (partial-sum nats))

(stream-head rr 10)
;Value 23: (1 3 6 10 15 21 28 36 45 55)


;; Exercise 3.56

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

(define (merge-stream s1 s2)
  (cond ((stream-null? s1) s2)
	((stream-null? s2) s1)
	(else
	 (let ((s1car (stream-car s1))
	       (s2car (stream-car s2)))
	   (cond ((< s1car s2car) (cons-stream
				   s1car
				   (merge-stream (stream-cdr s1) s2)))
		 ((> s1car s2car) (cons-stream
				   s2car
				   (merge-stream s1 (stream-cdr s2))))
		 (else (cons-stream s1car (merge-stream
					   (stream-cdr s1)
					   (stream-cdr s2)))))))))

(define S (cons-stream 1 (merge-stream (merge-stream (scale-stream S 2) (scale-stream S 3))
				       (scale-stream S 5))))

(stream-head S 20)
;Value 25: (1 2 3 4 5 6 8 9 10 12 15 16 18 20 24 25 27 30 32 36)

(stream-ref S 1000)
;Value: 51840000

(stream-ref S 99998)
;Value: 290237644800000000000000000000000000000


;; Exercise 3.57
(define fibs
  (cons-stream 0 (cons-stream 1 (add-stream (stream-cdr fibs) fibs))))

(stream-head fibs 10)
;Value 27: (0 1 1 2 3 5 8 13 21 34)

(stream-ref fibs 10000)  ; result appear immediately
;Value: 33644764876431783266621612005107543310302148460680063906564769974680081442166662368155595513633734025582065332680836159373734790483865268263040892463056431887354544369559827491606602099884183933864652731300088830269235673613135117579297437854413752130520504347701602264758318906527890855154366159582987279682987510631200575428783453215515103870818298969791613127856265033195487140214287532698187962046936097879900350962302291026368131493195275630227837628441540360584402572114334961180023091208287046088923962328835461505776583271252546093591128203925285393434620904245248929403901706233888991085841065183173360437470737908552631764325733993712871937587746897479926305837065742830161637408969178426378624212835258112820516370298089332099905707920064367426202389783111470054074998459250360633560933883831923386783056136435351892133279732908133732642652633989763922723407882928177953580570993691049175470808931841056146322338217465637321248226383092103297701648054726243842374862411453093812206564914032751086643394517512161526545361333111314042436854805106765843493523836959653428071768775328348234345557366719731392746273629108210679280784718035329131176778924659089938635459327894523777674406192240337638674004021330343297496902028328145933418826817683893072003634795623117103101291953169794607632737589253530772552375943788434504067715555779056450443016640119462580972216729758615026968443146952034614932291105970676243268515992834709891284706740862008587135016260312071903172086094081298321581077282076353186624611278245537208532365305775956430072517744315051539600905168603220349163222640885248852433158051534849622434848299380905070483482449327453732624567755879089187190803662058009594743150052402532709746995318770724376825907419939632265984147498193609285223945039707165443156421328157688908058783183404917434556270520223564846495196112460268313970975069382648706613264507665074611512677522748621598642530711298441182622661057163515069260029861704945425047491378115154139941550671256271197133252763631939606902895650288268608362241082050562430701794976171121233066073310059947366875


;; Exercise 3.58

(define (expand num den radix)
  (cons-stream
   (quotient (* num radix) den)
   (expand (remainder (* num radix) den)
	   den
	   radix)))

(define r (expand 1 7 10))
(stream-head r 10)
;Value 28: (1 4 2 8 5 7 1 4 2 8)

(stream-head (expand 3 8 10) 10)
;Value 29: (3 7 5 0 0 0 0 0 0 0)


;; Exercise 3.59
;;; a

(define (integrate-series stream)
  (define (iter n ss)
    (cons-stream (/ (stream-car ss) n)
		 (iter (+ n 1) (stream-cdr ss))))
  (iter 1 stream))


(stream-head (integrate-series nats) 20)
;Value 31: (1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)

(define exp-series
  (cons-stream 1 (integrate-series exp-series)))

(stream-head exp-series 10)
;Value 34: (1 1 1/2 1/6 1/24 1/120 1/720 1/5040 1/40320 1/362880)


(define cosin-series
  (cons-stream 1 (stream-map - (integrate-series sine-series))))

(define sine-series
  (cons-stream 0 (integrate-series cosin-series)))

(stream-head cosin-series 10)
;Value 39: (1 0 -1/2 0 1/24 0 -1/720 0 1/40320 0)

(stream-head sine-series 10)
;Value 40: (0 1 0 -1/6 0 1/120 0 -1/5040 0 1/362880)

;; Exercise 3.60

(define (add-series . args)
  (apply stream-map + args))

(define (mul-series s1 s2)
  (cons-stream (* (stream-car s1) (stream-car s2))
	       (add-series (scale-stream (stream-cdr s1) (stream-car s2))
			   (scale-stream (stream-cdr s2) (stream-car s1))
			   (cons-stream 0 (mul-series (stream-cdr s1) (stream-cdr s2))))))

(define one (add-series (mul-series sine-series sine-series)
			(mul-series cosin-series cosin-series)))

(stream-head one 10)
;Value 54: (1 0 0 0 0 0 0 0 0 0)

;; Exercise 3.61
(define (invert-unit-series stream)
  (cons-stream 1 (stream-map - (mul-series (stream-cdr stream) (invert-unit-series stream)))))

(define exp-invert (invert-unit-series exp-series))

(stream-head exp-invert 10)
;Value 55: (1 -1 1/2 -1/6 1/24 -1/120 1/720 -1/5040 1/40320 -1/362880)


;; Exercise 3.62

(define (div-series s1 s2)
  (if (= (stream-car s2) 0)
      (error "Division of zero series")
      (let ((inv (invert-unit-series s2)))
	(mul-series s1 inv))))

(define tan-series
  (div-series sine-series cosin-series))

(stream-head tan-series 10)
;Value 56: (0 1 0 1/3 0 2/15 0 17/315 0 62/2835)

;; Exercise 3.63

(define (naive-sqrt-stream x)
  (cons-stream 1.0 (stream-map (lambda (guess)
				 (sqrt-improve guess x))
			       (naive-sqrt-stream x))))
(stream-head (naive-sqrt-stream 2.0) 5)
;Value 20: (1. 1.5 1.4166666666666665 1.4142156862745097 1.4142135623746899)


;; Exercise 3.64
(define (stream-limit s tolerance)
  (let ((s0 (stream-ref s 0))
	(s1 (stream-ref s 1)))
    (if (< (abs (- s0 s1)) tolerance)
	s1
	(stream-limit (stream-cdr s) tolerance))))

(define (my-sqrt x tolerance)
  (stream-limit (sqrt-stream x) tolerance))

(my-sqrt 2.0 0.0001)
;Value: 1.4142135623746899


;; Exercise 3.65

(define (ln2-summands n)
  (cons-stream (/ 1.0 n)
	       (stream-map - (ln2-summands (+ n 1)))))

(define ln2-stream
  (partial-sum (ln2-summands 1)))

(stream-head ln2-stream 5)
;Value 21: (1. .5 .8333333333333333 .5833333333333333 .7833333333333333)

(stream-head (accelerated-sequence euler-transform ln2-stream) 5)
;Value 23: (1. .7 .6932773109243697 .6931488693329254 .6931471960735491)


;; Exercise 3.66 TODO

;; Exercise 3.67

(define (pairs-all s t)
  (cons-stream
   (list (stream-car s) (stream-car s))
   (interleave
    (interleave
     (stream-map (lambda (x) (list (stream-car s) x))
		 (stream-cdr t))
     (stream-map (lambda (x) (list x (stream-car s)))
		 (stream-cdr s)))
    (pairs-all (stream-cdr s) (stream-cdr t)))))

(stream-head (pairs-all nats nats) 10)
;Value 22: ((0 0) (0 1) (1 1) (1 0) (1 2) (0 2) (2 2) (2 0) (2 1) (0 3))


;; Exercise 3.68
(define (pairs-louis s t)
  (interleave
   (stream-map (lambda (x) (list (stream-car s) x))
	       t)
   (pairs-louis (stream-cdr s) (stream-cdr t))))

(stream-head (pairs-louis nats nats) 10)
;Aborting!: maximum recursion depth exceeded


;; Exercise 3.69

(define ints (from 1))

(define (triples s t u)
  (let ((s0 (stream-car s))
	(t0 (stream-car t))
	(u0 (stream-car u)))
    (cons-stream
     (list s0 t0 u0)
     (interleave
      (stream-cdr
       (stream-map (lambda (x) (list s0 (car x) (cadr x)))
		   (pairs t u)))
      (triples (stream-cdr s) (stream-cdr t) (stream-cdr u))))))

(stream-head (triples nats nats nats) 10)
;Value 25: ((0 0 0) (0 0 1) (1 1 1) (0 1 1) (1 1 2) (0 0 2) (2 2 2) (0 1 2) (1 2 2) (0 0 3))


(stream-head
 (stream-filter (lambda (x)
		  (= (+ (square (car x))
			(square (cadr x)))
		     (square (caddr x))))
		(triples ints ints ints))
 3)
;Value 28: ((3 4 5) (6 8 10) (5 12 13))


;; Exercise 3.70

(define (from n)
  (cons-stream n (from (+ n 1))))

(define nats (from 0))

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
	((stream-null? s2) s1)
	((< (weight (stream-car s1))
	    (weight (stream-car s2)))
	 (cons-stream (stream-car s1)
		      (merge-weighted (stream-cdr s1) s2 weight)))
	(else (cons-stream (stream-car s2)
			   (merge-weighted s1 (stream-cdr s2) weight)))))

(define (pairs-weight s t weight)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (merge-weighted
    (stream-map (lambda (x) (list (stream-car s) x))
		(stream-cdr t))
    (pairs-weight (stream-cdr s) (stream-cdr t) weight)
    weight)))

;; problem a
(stream-head (pairs-weight nats nats (lambda (x) (apply + x))) 10)
;Value 14: ((0 0) (0 1) (1 1) (0 2) (1 2) (0 3) (2 2) (1 3) (0 4) (2 3))

(stream-head (pairs nats nats) 10)
;Value 15: ((0 0) (0 1) (1 1) (0 2) (1 2) (0 3) (2 2) (0 4) (1 3) (0 5))


;; problem b
(stream-head
 (pairs-weight
  nats
  nats
  (lambda (pair)
    (let ((x (car pair))
	  (y (cadr pair)))
      (+ (* 2 x) (* 3 y) (* 5 x y))))) 10)
;Value 20: ((0 0) (0 1) (0 2) (0 3) (1 1) (0 4) (0 5) (1 2) (0 6) (0 7))

;; Exercise 3.71

(define (ramanujan-weight pair)
  (let ((x (car pair))
	(y (cadr pair)))
    (+ (* x x x) (* y y y))))

(define (ramanujan-stream s)
  (if (= (ramanujan-weight (stream-car s)) 
	 (ramanujan-weight (stream-car (stream-cdr s))))
      (cons-stream
       (ramanujan-weight (stream-car s))
       (ramanujan-stream (stream-cdr s)))
      (ramanujan-stream (stream-cdr s))))

(stream-head
 (ramanujan-stream
  (pairs-weight nats nats ramanujan-weight)) 10)
;Value 22: (1729 4104 13832 20683 32832 39312 40033 46683 64232 65728)


;; Exercise 3.72

(define (ramanujan-stream-tuple s)
  (if (= (ramanujan-weight (stream-car s))
	 (ramanujan-weight (stream-car (stream-cdr s)))
	 (ramanujan-weight (stream-car (stream-cdr (stream-cdr s)))))
      (cons-stream
       (ramanujan-weight (stream-car s))
       (ramanujan-stream-tuple (stream-cdr (stream-cdr s))))
      (ramanujan-stream-tuple (stream-cdr s))))

(stream-head
 (ramanujan-stream-tuple
  (pairs-weight nats nats ramanujan-weight)) 2)
;Value 23: (87539319 119824488)


;; Exercise 3.73
(define (add-streams s t)
  (stream-map + s t))

(define (scale-stream s factor)
  (stream-map (lambda (x) (* x factor)) s))

(define (integral integrand initial-value dt)
  (define int
    (cons-stream initial-value
		 (add-stream (scale-stream integrand dt)
			     int)))
  int)

(define (RC R C dt)
  (lambda (s v0)
    (add-streams
     (scale-stream s R)
     (integral (scale-stream s (/ 1.0 C)) v0 dt))))

(define RC1 (RC 5 1 0.5))

(RC1 nats 3)
;Value 26: (3 . #[promise 27])


;; Exercise 3.74

(define zero-crossings
  (stream-map sign-change-dector
	      sense-data
	      (cons-stream 0 sense-data)))


;; Exercise 3.75

(define (make-zero-crossings input-stream last-value last-signal)
  (let ((aptv (/ (+ (stream-car input-stream)
		    last-value) 2.0)))
    (cons-stream
     (sign-change-detector aptv last-signal)
     (make-zero-crossings
      (stream-cdr input-stream)
      (stream-car input-stream)
      aptv))))

;; Exercise 3.76

(define (smooth s)
  (cons-stream
   (/ (+ (stream-car s)
	 (stream-car (stream-cdr s))) 2.0)
   (smooth (stream-cdr s))))

(stream-head (smooth nats) 10) 
;Value 30: (.5 1.5 2.5 3.5 4.5 5.5 6.5 7.5 8.5 9.5)

