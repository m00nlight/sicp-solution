;; Section 2.1.1
(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y))
	       (* (numer y) (denom x)))
	    (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
	       (* (numer y) (denom x)))
	    (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
	    (* (denom x) (denom y))))

(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
	    (* (denom x) (numer y))))

(define (equal-rat? x y)
  (= (* (numer x) (denom y))
     (* (denom x) (numer y))))


(define (make-rat n d) (cons n d))
(define (numer x) (car x))
(define (denom x) (cdr x))

(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))

(define one-half (make-rat 1 2))
(print-rat one-half)

(define one-third (make-rat 1 3))
(print-rat (add-rat one-half one-third))
; 5/6

(print-rat (add-rat one-third one-third))
; 6/9


(define (make-rat n d)
  (let ((g (gcd n d)))
    (cons (/ n g) (/ d g))))


(print-rat (add-rat one-third one-third))
; 2/3


;;; Section 2.1.3

(define (my-cons x y)
  (define (dispatch m)
    (cond ((= m 0) x)
	  ((= m 1) y)
	  (else (error "Argument not 0 or 1: CONS" m))))
  dispatch)

(define (my-car z) (z 0))
(define (my-cdr z) (z 1))

(define tt (my-cons 2 3))

(my-car tt)
;Value: 2
(my-cdr tt)
;Value: 3


;;; Section 2.1.4
(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval
   x
   (make-interval (/ 1.0 (upper-bound y))
		  (/ 1.0 (lower-bound y)))))



;;; Section 2.2.1

(define one-through-four (list 1 2 3 4))

(car one-through-four)
;Value: 1

(cdr one-through-four)
;Value 13: (2 3 4)

(null? one-through-four)


;;; Section 2.2.2

(define (count-leaves x)
  (cond ((null? x) 0)
	((not (pair? x)) 1)
	(else (+ (count-leaves (car x))
		 (count-leaves (cdr x))))))

(count-leaves '(2 (1 3) 4 5 (6 7)))
; => 7

(define (scale-tree tree factor)
  (cond ((null? tree) '())
	((not (pair? tree)) (* tree factor))
	(else (cons (scale-tree (car tree) factor)
		    (scale-tree (cdr tree) factor)))))


(scale-tree (list 1 (list 2 (list 3 4) 5) (list 6 7)) 10)
;Value 16: (10 (20 (30 40) 50) (60 70))

(define (scale-tree tree factor)
  (map (lambda (sub-tree)
	 (if (pair? sub-tree)
	     (scale-tree sub-tree factor)
	     (* sub-tree factor)))
       tree))

(scale-tree (list 1 (list 2 (list 3 4) 5) (list 6 7)) 10)
;Value 17: (10 (20 (30 40) 50) (60 70))


;;; Section 2.2.3

(map square (list 1 2 3 4))


(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))

(enumerate-interval 2 7)
;Value 26: (2 3 4 5 6 7)


(define (enumerate-tree tree)
  (cond ((null? tree) '())
	((not (pair? tree)) (list tree))
	(else (append (enumerate-tree (car tree))
		      (enumerate-tree (cdr tree))))))

(enumerate-tree (list 1 (list 2 (list 3 4)) 5))
;Value 27: (1 2 3 4 5)

(accumulate append
	    '()
	    (map (lambda (i)
		   (map (lambda (j) (list i j))
			(enumerate-interval 1 (- i 1))))
		 (enumerate-interval 1 4)))

;Value 29: ((2 1) (3 1) (3 2) (4 1) (4 2) (4 3))


(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (permutations s)
  (if (null? s)
      '()
      (flatmap (lambda (x)
		 (map (lambda (p) (cons x p))
		      (permutations (my-remove x s))))
	       s)))

(define (my-remove item sequence)
  (filter (lambda (x) (not (= x item))) sequence))

(permutations '(1 2 3 4))

(flatmap (lambda (x) (+ x 1)) '(1 2 3 4))


;;; section 2.2.4


(define (right-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
	(beside painter (below smaller smaller)))))

(define (corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
	    (right (right-split painter (- n 1))))
	(let ((top-left (beside up up))
	      (bottom-right (below right right))
	      (corner (corner-split painter (- n 1))))
	  (beside (below painter top-left)
		  (below bottom-right corner))))))


(define (square-of-four tl tr bl br)
  (lambda (painter)
    (let ((top (beside (tl painter) (tr painter)))
	  (bottom (beside (bl painter) (br painter))))
      (below bottom top))))

(define (flipped-pairs painter)
  (let ((combine4 (square-of-four identity flip-vert
				  identity flip-vert)))
    (combine4 painter)))

(define (square-limit painter n)
  (let ((combine4 (square-of-four flip-horiz identity
				  rotate180 flip-vert)))
    (combine4 (corner-split painter n))))


(define (frame-coord-map frame)
  (lambda (v)
    (add-vector
     (origin-frame frame)
     (add-vector (scale-vect (xcor-vector v)
			     (edge1-frame frame))
		 (scale-vect (ycor-vector v)
			     (edge2-frame frame))))))


(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line
	((frame-coord-map frame)
	 (start-segment segment))
	((frame-coord-map frame)
	 (end-segment segment))))
     segment-list)))


;; section 2.3

;;; section 2.3.1

(car '(a b c))
;Value: a

(cdr '(a b c))
;Value: (b c)

(quote (a b c))
;Value 14: (a b c)
'(a b c)
;Value 15: (a b c)

(quote '(a b c))
;Value 16: (quote (a b c))

(equal? '(a b c) (quote (a b c)))
;Value: #t

(define (memq item x)
  (cond ((null? x) false)
	((eq? item (car x)) x)
	(else (memq item (cdr x)))))

(memq 'apple '(pear banana prune))
;Value: #f

(memq 'apple '(x (apple sauce) y apple pear))
;Value 17: (apple pear)

;;; section 2.3.2

(define (deriv exp var)
  (cond ((number? exp) 0)
	((variable? exp) (if (same-variable? exp var) 1 0))
	((sum? exp) (make-sum (deriv (addend exp) var)
			      (deriv (augend exp) var)))
	((product? exp) (make-sum
			 (make-product (multiplier exp)
				       (deriv (multiplicand exp) var))
			 (make-product (deriv (multiplier exp) var)
				       (multiplicand exp))))
	(else (error "unknow expression type: DERIV" exp))))


(define (variable? x) (symbol? x))
(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

(define (sum? x) (and (pair? x) (eq? (car x) '+)))
(define (addend s) (cadr s))
(define (augend s) (caddr s))
(define (product? x) (and (pair? x) (eq? (car x) '*)))
(define (multiplier p) (cadr p))
(define (multiplicand p) (caddr p))

(define (make-sum a1 a2) (list '+ a1 a2))
(define (make-product m1 m2) (list '* m1 m2))

(deriv '(+ x 3) 'x)
;Value 23: (+ 1 0)

(deriv '(* x y) 'x)
;Value 26: (+ (* x 0) (* 1 y))

(deriv '(* (* x y) (+ x 3)) 'x)
;Value 27: (+ (* (* x y) (+ 1 0)) (* (+ (* x 0) (* 1 y)) (+ x 3)))

(define (=number? exp num) (and (number? exp) (= exp num)))

(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
	((=number? a2 0) a1)
	((and (number? a1) (number? a2)) (+ a1 a2))
	(else (list '+ a1 a2))))

(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
	((=number? m1 1) m2)
	((=number? m2 1) m1)
	((and (number? m1) (number? m2)) (* m1 m2))
	(else (list '* m1 m2))))

(deriv '(+ x 3) 'x)
;Value: 1

(deriv '(* x y) 'x)
;Value: y

(deriv '(* (* x y) (+ x 3)) 'x)
;Value 30: (+ (* x y) (* y (+ x 3)))


;;; Section 2.3.3

;;;; Set as unordered list

(define (element-of-set? x set)
  (cond ((null? set) false)
	((equal? x (car set)) true)
	(else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (if (element-of-set? x set)
      set
      (cons x set)))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
	((element-of-set? (car set1) set2)
	 (cons
	  (car set1)
	  (intersection-set (cdr set1) set2)))
	(else (intersection-set (cdr set1) set2))))


;;;; Set as ordered list

(define (element-of-set? x set)
  (cond ((null? false))
	((= x (car set)) true)
	((< x (car set)) false)
	(else (element-of-set? x (cdr set)))))

(define (intersection-set set1 set2)
  (if (or (null? set1) (null? set2))
      '()
      (let ((x1 (car set1)) (x2 (car set2)))
	(cond ((= x1 x2)
	       (cons x1 (intersection-set (cdr set1) (cdr set2))))
	      ((< x1 x2)
	       (intersection-set (cdr set1) set2))
	      ((< x2 x1)
	       (intersection-set set1 (cdr set2)))))))



;;;; Set as binary-search-tree
(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (element-of-set? x set)
  (cond ((null? set) false)
	((= x (entry set)) true)
	((< x (entry set))
	 (element-of-set x (left-branch set)))
	((> x (entry set))
	 (element-of-set? x (right-branch set)))))

(define (adjoin-set x set)
  (cond ((null? set) (make-tree x '() '()))
	((= x (entry set)) set)
	((< x (entry set))
	 (make-tree (entry set)
		    (adjoin-set x (left-branch set))
		    (right-branch set)))
	((> x (entry set))
	 (make-tree (entry set) (left-branch set)
		    (adjoin-set x (right-branch set))))))


;;;; sets and information retrieval

(define (lookup given-key set-of-records)
  (cond ((null? set-of-records) false)
	((equal? given-key (key (car set-of-records))) (car set-of-records))
	(else (lookup given-key (cdr set-of-records)))))


;;; Section 2.3.4

(define (make-leaf symbol weight)
  (list 'leaf symbol weight))

(define (leaf? object)
  (eq? (car object) 'leaf))

(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))

(define (make-code-tree left right)
  (list left
	right
	(append (symbols left)
		(symbols right))
	(+ (weight left)
	   (weight right))))

(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))

(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))

(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (if (null? bits)
	'()
	(let ((next-branch
	       (choose-branch (car bits) current-branch)))
	  (if (leaf? next-branch)
	      (cons (symbol-leaf next-branch)
		    (decode-1 (cdr bits) tree))
	      (decode-1 (cdr bits) next-branch)))))
  (decode-1 bits tree))

(define (choose-branch bit branch)
  (cond ((= bit 0) (left-branch branch))
	((= bit 1) (right-branch branch))
	(else (error "bad bit: CHOOSE-BRANCH" bit))))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
	((< (weight x) (weight (car set))) (cons x set))
	(else (cons (car set)
		    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
	(adjoin-set (make-leaf (car pair)
			       (cadr pair))
		    (make-leaf-set (cdr pairs))))))


;; section 2.4

;;; section 2.4.1

(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum: TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum: CONTENT" datum)))

;;;; manipulate for rectangle representation

(define (real-part-rectangle z) (car z))
(define (imag-part-rectangle z) (cdr z))
(define (magnitude-rectangle z)
  (sqrt (+ (square (real-part-rectangle z))
	   (square (imag-part-rectangle z)))))
(define (angle-rectangle z)
  (atan (imag-part-rectangle z)
	(real-part-rectangle z)))

;;;; manipulate for polar representation

(define (magnitude-polar z) (car z))
(define (angle-polar z) (cdr z))
(define (real-part-polar z)
  (* (magnitude-polar z) (cos (angle-polar z))))
(define (imag-part-polar z)
  (* (magnitude-polar z) (sin (angle-polar z))))


;;;; tagged data part

(define (make-from-real-imag-rectangle x y)
  (attach-tag 'rectangle (cons x y)))

(define (make-from-mag-ang-polar x y)
  (attach-tag 'polar (cons x y)))

(define (rectangle? z) (eq? (type-tag z) 'rectangle))
(define (polar? z) (eq? (type-tag z) 'polar))

(define (real-part z)
  (cond ((rectangle? z) (real-part-rectangle (contents z)))
	((polar? z) (real-part-polar (contents z)))
	(else (error "Unknow type: REAL-PART" z))))

(define (imag-part z)
  (cond ((rectangle? z) (imag-part-rectangle (contents z)))
	((polar? z) (imag-part-polar (contents z)))
	(else (error "Unknow type: IMAG-PAET" z))))

(define (magnitude z)
  (cond ((rectangle? z) (magnitude-rectangle (contents z)))
	((polar? z) (magnitude-polar (contents z)))
	(else (error "Unknow type: MAGNITUDE" z))))

(define (angle z)
  (cond ((rectangle? z) (angle-rectangle (contents z)))
	((polar? z) (angle-polar (contents z)))
	(else (error "Unknow type: ANGLE" z))))

;;;; rewrite the four function above
(define (normal z f1 f2 error-string)
  (cond ((rectangle? z) (f1 (contents z)))
	((polar? z) (f2 (contents z)))
	(else (error "Unknow type :" error-string z))))

(define (real-part z)
  (normal z real-part-rectangle real-part-polar "REAL-PART"))

(define (imag-part z)
  (normal z imag-part-rectangle imag-part-polar "IMAG-PAER"))

(define (angle z)
  (normal z angle-rectangle angle-polar "ANGLE"))

(define (magnitude z)
  (normal z magnitude-rectangle magnitude-polar "MAGNITUDE"))


;;; section 2.4.3

(define *op-table* (make-equal-hash-table))
(define (put op type proc)
  (hash-table/put! *op-table* (list op type) proc))
(define (get op type)
  (hash-table/get *op-table* (list op type) '()))

;;;; procedure from section 2.4.1
(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum: TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum: CONTENT" datum)))

;;;; representation of rectangular package
(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car z))
  (define (imag-part z) (cdr z))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (sqrt (+ (square (real-part z))
	     (square (imag-part z)))))
  (define (angle z)
    (atan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a)
    (cons (* r (cos a)) (* r (sin a))))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangle x))
  (begin
    (put 'real-part 'rectangular real-part)
    (put 'imag-part 'rectangular imag-part)
    (put 'magnitude 'rectangular magnitude)
    (put 'make-from-real-imag 'rectangular
	 (lambda (x y) (tag (make-from-real-imag x y))))
    (put 'make-from-mag-ang 'rectangle
	 (lambda (r a) (tag (make-from-mag-ang r a))))
    'done))

(install-rectangular-package)  ;;; install rectangular representation to system

(get 'real-part 'rectangular)
;Value 23: #[compound-procedure 23 real-part]



(define (install-polar-package)
  ;;internal procedure
  (define (magnitude z) (car z))
  (define (angle z) (cdr z))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z)
    (* (magnitude z) (cos (angle z))))
  (define (imag-part z)
    (* (magnitude z) (sin (angle z))))
  (define (make-from-real-imag x y)
    (cons (sqrt (+ (square x) (square y)))
	  (atan y x)))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (begin
    (put 'real-part 'polar real-part)
    (put 'imag-part 'polar imag-part)
    (put 'magnitude 'polar magnitude)
    (put 'angle 'polar angle)
    (put 'make-from-real-imag 'polar
	 (lambda (x y) (tag (make-from-real-imag x y))))
    (put 'make-from-mag-ang 'polar
	 (lambda (r a) (tag (make-from-mag-ang r a))))
    'done))

(install-polar-package)    ;;; install the polar representation to system
(get 'real-part 'polar)
;Value 24: #[compound-procedure 24 real-part]

((get 'real-part 'rectangular) (cons 1 2))
;Value: 1

(define (apply-generic op arg)
  (let ((type (type-tag arg)))
    (let ((proc (get op type)))
      (if proc
	  (proc (contents arg))
	  (error "No method for these types: APPLY-GENERIC:"
		 (list op type))))))

(define (real-part z) (apply-generic 'real-part z))
(define (imag-part z) (apply-generic 'imag-part z))
(define (magnitude z) (apply-generic 'magnitude z))
(define (angle z) (apply-generic 'angle z))

(define c1 (cons 'rectangular (cons 1 2)))
(define c2 (cons 'polar (cons 4 0.718)))

(real-part c1)
;Value: 1
(real-part c2)
;Value: 3.012491975978806
(+ (real-part c1) (real-part c2))
;Value: 3.012491975978806


;;; message passing style
(define (make-from-real-imag x y)
  (define (dispatch op)
    (cond ((eq? op 'real-part) x)
	  ((eq? op 'imag-part y))
	  ((eq? op 'magnitude)
	   (sqrt (+ (square x) (square y))))
	  ((eq? op 'angle (atan y x)))
	  (else (error "Unknow op: MAKE-FROM-REAL-IMAG:" op))))
  dispatch)

(define (apply-generic op arg) (arg op))

;;; section 2.5.1


(define *op-table* (make-equal-hash-table))
(define (put op type proc)
  (hash-table/put! *op-table* (list op type) proc))
(define (get op type)
  (hash-table/get *op-table* (list op type) '()))

;;;; procedure from section 2.4.1
(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum: TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum: CONTENT" datum)))

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (display proc)
      (newline)
      (display type-tags)
      (if proc
	  (apply proc (map contents args))
	  (error "No method for these types: APPLY-GENERIC:"
		 (list op type))))))


;;;; generic arithmetic procedures 
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))

;;;; number arithmetic package
(define (install-scheme-number-package)
  (define (tag x) (attach-tag 'scheme-number x))
  (begin
    (put 'add '(scheme-number scheme-number)
	 (lambda (x y) (tag (+ x y))))
    (put 'sub '(scheme-number scheme-number)
	 (lambda (x y) (tag (- x y))))
    (put 'mul '(scheme-number scheme-number)
	 (lambda (x y) (tag (* x y))))
    (put 'div '(scheme-number scheme-number)
	 (lambda (x y) (tag (/ x y))))
    (put 'make 'scheme-number (lambda (x) (tag x)))
    (put 'equ? '(scheme-number)
	 (lambda (x y) (tag (= x y))))
    (put '=zero? '(scheme-number)
	 (lambda (x) (tag (= x 0))))
    'done))

(install-scheme-number-package)

(define (make-scheme-number n)
  ((get 'make 'scheme-number) n))

(make-scheme-number 10)
;Value 13: (scheme-number . 10)


;;;; rational number package
(define (install-rational-package)
  ;;internal procedures
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (make-rat n d)
    (let ((g (gcd n d)))
      (cons (/ n g) (/ d g))))
  (define (add-rat x y)
    (make-rat (+ (* (numer x) (denom y))
		 (* (numer y) (denom x)))
	      (* (denom x) (denom y))))
  (define (sub-rat x y)
    (make-rat (- (* (numer x) (denom y))
		 (* (numer y) (denom x)))
	      (* (denom x) (denom y))))
  (define (mul-rat x y)
    (make-rat (* (numer x) (numer y))
	      (* (denom x) (denom y))))
  (define (div-rat x y)
    (make-rat (* (numer x) (denom y))
	      (* (denom x) (numer y))))
  (define (equ-rat? x y)
    (and (= (numer x) (numer y))
	 (= (denom x) (denom y))))
  (define (=zero-rat? x)
    (= (numer x) 0))
  ;;interface to the rest of the system
  (define (tag x) (attach-tag 'rational x))
  (begin
    (put 'add '(rational rational)
	 (lambda (x y) (tag (add-rat x y))))
    (put 'sub '(rational rational)
	 (lambda (x y) (tag (sub-rat x y))))
    (put 'mul '(rational rational)
	 (lambda (x y) (tag (mul-rat x y))))
    (put 'div '(rational rational)
	 (lambda (x y) (tag (div-rat x y))))
    (put 'make 'rational
	 (lambda (n d) (tag (make-rat n d))))
    (put 'equ? '(rational rational)
	 (lambda (x y) (tag (equ-rat? x y))))
    (put '=zero? '(rational)
	 (lambda (x) (tag (=zero-rat? x))))
    'done))

(install-rational-package)

(define (make-rational n d)
  ((get 'make 'rational) n d))

(make-rational 3 4)
;Value 14: (rational 3 . 4)


;;;; complex number package
(define (install-complex-package)
  ;;construct method
  (define (make-from-real-imag x y)
    ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a)
    ((get 'make-from-mag-ang 'polar) r a))
  (define (add-complex z1 z2)
    (make-from-real-imag (+ (real-part z1) (real-part z2))
			 (+ (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2)
    (make-from-real-imag (- (real-part z1) (real-part z2))
			 (- (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2)
    (make-from-mag-ang (* (magnitude z1) (magnitude z2))
		       (+ (angle z1) (angle z2))))
  (define (div-complex z1 z2)
    (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
		       (- (angle z1) (angle z2))))
  (define (equ-complex? z1 z2)
    (and (= (real-part z1) (real-part z2))
	 (= (imag-part z1) (imag-part z2))))
  (define (=zero-complex? z)
    (and (= (real-part z) 0) (= (imag-part z) 0)))
  ;;interface to the system
  (define (tag z) (attach-tag 'complex z))
  (begin
    (put 'add '(complex complex)
	 (lambda (z1 z2) (tag (add-complex z1 z2))))
    (put 'sub '(complex complex)
	 (lambda (z1 z2) (tag (sub-complex z1 z2))))
    (put 'mul '(complex complex)
	 (lambda (z1 z2) (tag (mul-complex z1 z2))))
    (put 'div '(complex complex)
	 (lambda (z1 z2) (tag (div-complex z1 z2))))
    (put 'make-from-real-imag 'complex
	 (lambda (x y) (tag (make-from-real-imag x y))))
    (put 'make-from-mag-ang 'complex
	 (lambda (r a) (tag (make-from-mag-ang r a))))
    (put 'equ? '(complex complex)
	 (lambda (z1 z2) (tag (equ-complex? z1 z2))))
    (put '=zero? '(complex)
	 (lambda (z) (tag (=zero-complex? z))))
    'done))

(install-complex-package)

(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag 'complex) x y))
(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang 'complex) r a))

(make-complex-from-real-imag 3 4)
;Value 15: (complex rectangle 3 . 4)

;; Section 2.5.2

(define (scheme-number->complex n)
  (make-complex-from-real-imag (contents n) 0)
  (put-coercion 'scheme-number
		'complex
		scheme-number->complex))


(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
	  (apply proc (map contents args))
	  (if (= (length args) 2)
	      (let ((type1 (car type-tags))
		    (type2 (cadr type-tags))
		    (a1 (car args))
		    (a2 (cadr args)))
		(let ((t1->t2 (get-coercion type1 type2))
		      (t2->t1 (get-coercion type2 type1)))
		  (cond (t1->t2
			 (apply-generic op (t1->t2 a1) a2))
			(t2->t1
			 (apply-generic op a1 (t2->t1 a2)))
			(else (error "No method for these types"
				     (list op type-tags))))))
	      (error "No method for these types"
		     (list op type-tags)))))))

;; Section 2.5.3

;; TODO in the future with the whole 2.5 section

