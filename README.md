# This is my own solution to SICP

## About SICP

SICP stands for [Structure and Interpretation of Computer Programming](http://www.amazon.com/Structure-Interpretation-Computer-Programs-Edition/dp/0070004846/ref=sr_1_1?ie=UTF8&qid=1358301567&sr=8-1&keywords=SICP). It is the introduction to Programming in MIT since the 1980s. It use a Lisp dialect called scheme to teach the basic ideas of programming, and was widely considered as one of the best books and course to teach students programming. This course not only teach you specific programming language, but the basic concept and skills behind all programming languages.

## About This reposity

This is the solution to SICP Exercise of my own. The source file will only contain solution and
some comments to explain the solution. I may make edit some solution in this reposity [wiki](https://bitbucket.org/m00nlight/sicp-solution/wiki/Home).

All the code was written and run using [mit-scheme](http://www.gnu.org/software/mit-scheme/). You may can also using
[DrRacket](http://racket-lang.org) or [guile](http://www.gnu.org/software/guile/) to run this code, but may need some configuration of the mode of DrRacket.
