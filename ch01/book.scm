;; Newton's square root method

(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y) 2))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(sqrt 9)
; => 3.00009155413138

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
	(else else-clause)))

(define (sqrt-iter-bad-if guess x)
  (new-if (good-enough? guess x)
	  guess
	  (sqrt-iter-bad-if (improve guess x) x)))

(define (sqrt-bad-if x)
  (sqrt-iter-bad-if 1.0 x))

(sqrt-bad-if 9.0)
; => Aborting! maximum recursion depth exceeded

(sqrt 0.001)
; => .04124542607499115
(sqrt 0.0001)
; => .03230844833048122
(sqrt 0.0000001)
; => .03230844833048122
(sqrt 1e60)
; => not terminated

(define (good-enough-enhance? guess x)
  (< (abs (- (improve guess x) guess))
     (* guess 0.001)))

(define (sqrt-iter-enhance guess x)
  (begin
    (display guess)
    (newline)
    (if (good-enough-enhance? guess x)
	guess
	(sqrt-iter-enhance (improve guess x) x))))

(define (sqrt-enhance x)
  (sqrt-iter-enhance 1.0 x))

(sqrt-enhance 0.0000001)
;Value: 3.162477562740737e-4

(sqrt-enhance 1e60)
;Value: 1.0000788456669446e30



;;; section 1.2.2 tree recursion

;; fibonacci recursive function
(define (fib-rec n)
  (cond ((= n 0) 0)
	(( = n 1) 1)
	(else (+ (fib-rec (- n 1))
		 (fib-rec (- n 2))))))

(fib-rec 10)
;Value: 55

(fib-rec 20)
;Value: 6765

(fib-rec 30) 
;Value: 832040  ; already wait a while before answer came out

(define (fib-call n)
  (define (fib-iter a b count)
    (if (= count 0)
	b
	(fib-iter (+ a b) a (- count 1))))
  (fib-iter 1 0 n))

(fib-call 20)
;Value: 6765

(fib-call 30)
;Value: 832040 ;  answer came out immediately

;; counting changes

(define (count-change amount) (cc amount 5))
(define (cc amount kinds-of-coins)
  (cond ((= amount 0) 1)
	((or (< amount 0) (= kinds-of-coins 0)) 0)
	(else (+ (cc amount (- kinds-of-coins 1))
		 (cc (- amount (first-denomination kinds-of-coins)) kinds-of-coins)))))

(define (first-denomination kinds-of-coins)
  (cond ((= kinds-of-coins 1) 1)
	((= kinds-of-coins 2) 5)
	((= kinds-of-coins 3) 10)
	((= kinds-of-coins 4) 25)
	((= kinds-of-coins 5) 50)))

(count-change 100)
;Value: 292


(define (my-expt-rec b n)
  (if (= n 0)
      1
      (* b (my-expt-rec b (- n 1)))))

(my-expt-rec 2 3)


;; using closure feature
(define (my-expt b n)
  (define (my-expt-iter c product)
    (if (= c 0)
	product
	(my-expt-iter (- c 1) (* product b))))
  (my-expt-iter n 1))

(my-expt 2 3)


;; fast exponentiation
(define (fast-expt b n)
  (cond ((= n 0) 1)
	((even? n) (square (fast-expt b (/ n 2))))
	(else (* b (fast-expt b (- n 1))))))

(fast-expt 2 100)


(define (fib n)
  (define (fib-iter )))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor)n )n)
	((divides? test-divisor n) test-divisor)
	(else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(prime? 4) ; => #f
(prime? 91) ; => #f
(prime? 5) ; => #t

(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
	(else
	 (remainder (* base (expmod base (- exp 1) m)) m))))

(expmod 3 1000 21)
;Value : 18

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
	((fermat-test n) (fast-prime? n (- times 1)))
	(else false)))

(fast-prime? 21 4)



;;; section 1.3
(define (cube x) (* x x x))

(define (sum-integers a b)
  (if (> a b)
      0
      (+ a (sum-integers (+ a 1) b))))

(sum-integers 1 100)
;Value: 5050

(define (sum-cubes a b)
  (if (> a b)
      0
      (+ (cube a) (sum-cubes (+ a 1) b))))

(sum-cubes 1 100)
;Value: 25502500

(define (pi-sum a b)
  (if (> a b)
      0
      (+ (/ 1.0 (* a (+ a 2)))
	 (pi-sum (+ a 4) b))))

(pi-sum 1 41)
;Value: .38702019080795513

(define (sum-common term a next b)
  "term is a procedure calculate at integer a, and next is another procedure
to return the next integer of a"
  (if (> a b)
      0
      (+ (term a)
	 (sum-common term (next a) next b))))

(define (inc n) (+ n 1))
(define (identity x) x)
(define (sum-integers2 a b)
  (sum-common identity a inc b))

(sum-integers2 1 101)
;Value: 5151

(define (sum-cubes2 a b)
  (sum-common cube a inc b))
(sum-cubes2 1 100)
;Value: 25502500

(define (pi-sum2 a b)
  (define (pi-term x)
    (/ 1.0 (* x (+ x 2))))
  (define (pi-next x)
    (+ x 4))
  (sum-common pi-term a pi-next b))

(pi-sum2 1 41)
;Value: .38702019080795513


(define (integral f a b dx)
  (define (add-dx x)
    (+ x dx))
  (* (sum-common f (+ a (/ dx 2.0)) add-dx b)
     dx))

(integral cube 0 1 0.01)
;Value: .24998750000000042


;; section 1.3.2
(define (pi-sum a b)
  (sum-common (lambda (x) (/ 1.0 (* x (+ x 2))))
	      a
	      (lambda (x) (+ x 4))
	      b))

(pi-sum 1 1001)


;; section 1.3.3

(define (search f neg-point pos-point)
  (let ((mid-point (average neg-point pos-point)))
    (if (close-enough? neg-point pos-point)
	mid-point
	(let ((test-value (f mid-point)))
	  (cond ((positive? test-value)
		 (search f neg-point mid-point))
		((negative? test-value)
		 (search f mid-point pos-point))
		(else mid-point))))))

(define (close-enough? a b) (< (abs (- a b)) 0.001))

(define (half-interval-method f a b)
  (let ((a-value (f a))
	(b-value (f b)))
    (cond ((and (negative? a-value) (positive? b-value))
	   (search f a b))
	  ((and (negative? b-value) (positive? a-value))
	   (search f b a))
	  (else (error "Value are not of opssite sign" a b)))))

(half-interval-method sin 2.0 4.0)
;Value: 3.14111328125

(half-interval-method (lambda (x)
			(- (* x x x) (* 2 x) 3))
		      1.0
		      2.0)
;Value: 1.89306640625

(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? a b)
    (< (abs (- a b)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? next guess)
	  next
	  (try next))))
  (try first-guess))

(fixed-point cos 1.0)
;Value: .7390822985224024

(fixed-point (lambda (y) (+ (sin y) (cos y))) 1.0)
;Value: 1.2587315962971173

;; the following fixed-point program for sqrt does not converge
(define (sqrt-not-converge x)
  (fixed-point (lambda (y) (/ x y)) 3.0))

(sqrt-fix-point 2.0)

(define (sqrt-converge x)
  (fixed-point (lambda (y) (average y (/ x y))) 1.0))

(sqrt-converge 4.0)
;Value: 2.000000000000002

;;; Section 1.3.4

(define (average-damp f) (lambda (x) (average x (f x))))

(define (my-sqrt x)
  (fixed-point (average-damp (lambda (y) (/ x y))) 1.0))

(my-sqrt 5)
;Value: 2.236067977499978

(define (cube-root-2 x)
  (fixed-point (average-damp (lambda (y) (/ x (square y))))
	       1.0))

(cube-root-2 27)
;Value: 2.9999972321057697

;; netwoon's method
(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx)))

(define dx 0.00001)

(define (newton-transform g)
  (lambda (x) (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

(define (sqrt-newton x)
  (newtons-method (lambda (y) (- (square y) x)) 1.0))

(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))

(define (sqrt-2 x)
  (fixed-point-of-transform
   (lambda (y) (/ x y)) average-damp 1.0))

(sqrt-2 5)
;Value: 2.236067977499978

(define (sqrt-3 x)
  (fixed-point-of-transform
   (lambda (y) (- (square y) x)) newton-transform 1.0))

(sqrt-3 5)
;Value: 2.2360679775020436
